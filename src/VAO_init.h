#ifndef __VAO_INIT_H
#define __VAO_INIT_H

#ifdef __cplusplus
extern "C"{
#endif


#include <GL/gl.h>

GLuint createVAO(void);



#ifdef __cplusplus
}
#endif

#endif