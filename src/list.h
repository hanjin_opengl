#ifndef __list_H
#define __list_H

 
#ifdef __cplusplus
extern "C"{
#endif

#ifdef WIN32
#define inline __inline
#endif

struct list_head
{
	struct list_head *next;
	struct list_head *prev;
};

#define LIST_HEAD_INIT(head)  {&(head), &(head)}

#define LIST_HEAD(head) \
		struct list_head head = LIST_HEAD_INIT(head)
	  
static inline void init_list_head(struct list_head *head)
{
	head->next = head;
	head->prev = head;
}
#define new NEW
static inline void __list_add(struct list_head *new, struct list_head *prev, struct list_head *next)
{
	new->prev = prev;
	new->next = next;
	prev->next = new;
	next->prev = new;
}

static inline void list_add(struct list_head *new,struct list_head *head)
{
	__list_add(new, head, head->next);
}

static inline void list_add_tail(struct list_head *new,struct list_head *head)
{
	__list_add(new, head->prev, head);
}


static inline void __list_del(struct list_head *prev, struct list_head *next)
{
	prev->next = next;
	next->prev = prev;
}

static inline void list_del(struct list_head *list)
{
	__list_del(list->prev, list->next);
}

static inline void list_del_init(struct list_head *list)
{
	__list_del(list->prev, list->next);
	init_list_head(list);
}

static inline int list_empty(struct list_head *head)
{
	return head == head->next;
}

#define list_for_each(pos,head) \
		for(pos = (head)->next; pos != (head); pos = pos->next)

#define list_for_each_reverse(pos,head) \
		for(pos = (head)->prev; pos != (head); pos = pos->prev)

#define offset(type,member) \
		(char *)(&(((type*)0)->member))

#define list_entry(ptr, type, member) \
		(type *)((char *)ptr - offset(type,member)) 
	  
static inline struct list_head * pop_list(struct list_head *head)
{
	struct list_head *list = NULL;
	if(!list_empty(head))
	{
		list = head->next;
		list_del(list);
	}
	return list;
}

#if 0
#define push_list(new, head) list_add(new, head)
#endif

static inline void push_list(struct list_head *new, struct list_head *head)
{
	list_add(new, head);
}

#undef new

#ifdef __cplusplus
}
#endif

#endif
