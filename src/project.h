#ifndef __PROJECT_MY_H
#define __PROJECT_MY_H

#ifdef __cplusplus
extern "C" {
#endif

#include <GL/gl.h>
void PerspectiveM(GLdouble fovy, GLdouble aspect, GLdouble zNear, GLdouble zFar,GLdouble *);
void LookAtM(GLdouble eyex, GLdouble eyey, GLdouble eyez, GLdouble centerx, GLdouble centery, GLdouble centerz, GLdouble upx, GLdouble upy, GLdouble upz,GLdouble *m);
GLint ProjectM(GLdouble objx, GLdouble objy, GLdouble objz, const GLdouble modelMatrix[16], const GLdouble projMatrix[16], const GLint viewport[4], GLdouble *winx, GLdouble *winy, GLdouble *winz);
GLint UnProject4(GLdouble winx, GLdouble winy, GLdouble winz, GLdouble clipw, const GLdouble modelMatrix[16], const GLdouble projMatrix[16], const GLint viewport[4], GLclampd nearVal, GLclampd farVal, GLdouble *objx, GLdouble *objy, GLdouble *objz, GLdouble *objw);
void Translated(GLdouble m[4][4],double x,double y,double z);
void MultMatrixVecd(const GLdouble matrix[16], const GLdouble in[4], GLdouble out[4]);
void MultMatricesd(const GLdouble left[16], const GLdouble right[16], GLdouble out[16]);
void RotationMatrix44d(GLdouble m[16], double angle, double x, double y, double z);
void ScaleMatrix44d(GLdouble m[16], double x, double y, double z);

#ifdef __cplusplus
}
#endif

#endif
