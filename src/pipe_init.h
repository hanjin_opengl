#ifndef __PIPE_INIT_H
#define __PIPE_INIT_H


#ifdef __cplusplus
extern "C" {
#endif




#include <GL/gl.h>
enum matrixType { MODEL_MATRIX = 0, VIEW_MATRIX, PROJECT_MATRIX};

void init_pipe(void);
void pipeBindPositionLocation(void);
void pipeLinkProgram(void);
GLdouble * getMatrixP(enum matrixType type);
void updateModelMatrix(void);
void updateViewMatrix(void);
void updateProjectMatrix(void);
void setViewMatrix(GLdouble m[4][4]);
void texture_init(void);
#ifdef DEBUG
void printMatrix(GLdouble m[4][4]);
#else
#define printMatrix(m) {}
#endif
void fboBind(void);
void ScaleMatrix44d(GLdouble m[16], double x, double y, double z);
#ifdef __cplusplus
}
#endif

#endif
