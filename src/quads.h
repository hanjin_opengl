#ifndef __OBJECT_H
#define __OBJECT_H

#ifdef __cplusplus
extern "C"{
#endif

#include <GL/glew.h>
#include <GL/gl.h>

struct quads{
	GLuint vao;
	GLint inited;
	GLuint primitive_count;
	
	
	GLuint vertex;
	GLint vertex_inited;
	GLint vertex_used;

	GLuint color;
	GLint color_inited;
	GLint color_used;

	GLuint normal;
	GLint normal_inited;
	GLint normal_used;

	GLuint texture;
	GLint texture_inited;
	GLint texture_used;

	GLuint index;
	GLint index_inited;
	GLint index_used;
};


void quads_init(struct quads *object, GLuint primitiveCount,\
				 const GLdouble *vertex, GLuint vertexCount,\
				 const GLdouble *color, const GLdouble *normal,\
				 const GLdouble *texture, const GLuint *index);

void quads_draw(struct quads* object);

#ifdef __cplusplus
}
#endif

#endif