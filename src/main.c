//#include <windows.h>
#include <GL/glew.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>
#include <math.h>
#include <stdio.h>
#include "list.h"
#include "quads.h"

#include "cube.h"
#include "VAO_init.h"
#include "pipe_init.h"
#include "project.h"
//#pragma comment(lib,"glew32.lib")
//#pragma comment(lib,"glut32.lib")
//#pragma comment(lib,"openGl32.lib")
//#define DEBUG
#define ANGLE 20

static int width = 400, hight = 400;
static double centerX = .0, centerY = .0, centerZ = -0.0;
static double upX = 0.0, upY = 1.0, upZ = 0.0;
static double eyeX = 0.0, eyeY = 150.0, eyeZ = 150.0;
static int perspectiveNear = 10,perspectiveFar = 3400;

static void changeSize(int w, int h)
{
	width = w, hight = h;
	glViewport(0,0,w,h);

	PerspectiveM(ANGLE*1.0*h/400, w*1.0/h, perspectiveNear, perspectiveFar, getMatrixP(PROJECT_MATRIX) );
	updateProjectMatrix();
}





//#define GLUT_LEFT_BUTTON		0
//#define GLUT_MIDDLE_BUTTON	1
//#define GLUT_RIGHT_BUTTON		2
#define MOUSE_BUTTON_COUNT		3
//#define GLUT_DOWN				0
//#define GLUT_UP				1
#define MOUSE_KEY_COUNT			2


enum mouse_state{MOUSE_A = 0, MOUSE_B, MOUSE_C, MOUSE_D, MOUSE_E, MOUSE_F, MOUSE_NONE, MOUSE_MAX};
static char* mouseState[]={ "MOUSE_A", "MOUSE_B", "MOUSE_C", "MOUSE_D", "MOUNSE_E", "MOUSE_NONE", "MOUSE_MAX"};
static enum mouse_state mouse_state_table[MOUSE_MAX][MOUSE_BUTTON_COUNT][MOUSE_KEY_COUNT] = {
	{{MOUSE_NONE,MOUSE_A},{MOUSE_B,MOUSE_NONE},{MOUSE_NONE,MOUSE_A}},//a
	{{MOUSE_A,MOUSE_A},{MOUSE_NONE,MOUSE_F},{MOUSE_C,MOUSE_B}},//b
	{{MOUSE_A,MOUSE_A},{MOUSE_NONE,MOUSE_D},{MOUSE_NONE,MOUSE_E}},//c
	{{MOUSE_A,MOUSE_A},{MOUSE_D,MOUSE_D},{MOUSE_NONE,MOUSE_A}},//d
	{{MOUSE_A,MOUSE_A},{MOUSE_NONE,MOUSE_A},{MOUSE_C,MOUSE_NONE}},//e
	{{MOUSE_A,MOUSE_A},{MOUSE_NONE,MOUSE_A},{MOUSE_NONE,MOUSE_NONE}},//f
	{{MOUSE_A,MOUSE_A},{MOUSE_NONE,MOUSE_A},{MOUSE_NONE,MOUSE_A}}};//none

static enum mouse_state mouse_state = MOUSE_A;


static void mouseFunc(int button, int state, int x, int y)
{
	mouse_state = mouse_state_table[mouse_state][button][state];
	switch(mouse_state)
	{
	case MOUSE_F:
		if(1)//判断是否进行了缩放，平移或旋转操作，若没有进行，则执行焦点选择操作
		{}	//焦点选择操作
		mouse_state = MOUSE_A;
		break;
	}
	//printf("%s\n",mouseState[mouse_state]);
}



static int mouseX,mouseY;

static void rotate(int x, int y)
{
	GLdouble centerInModel[4],centerInView[4];
	GLdouble *matrixView,transMatrix[4][4],viewMatrix[4][4],rotateMatrix[4][4];
	GLfloat angleY,angleX;
	
	matrixView = (GLdouble*)getMatrixP(VIEW_MATRIX);
	centerInModel[0] = centerX,centerInModel[1] = centerY,centerInModel[2] =centerZ,centerInModel[3] = 1.0;
	printf("<<<<<<<<<<<<<<<<<<@center_in_model>>>>>>>>>>>>>>>>>%f,%f,%f\n",centerInModel[0],centerInModel[1],centerInModel[2]);
	MultMatrixVecd(matrixView, centerInModel, centerInView);
	
	
	Translated(transMatrix,-centerInView[0],-centerInView[1],-centerInView[2]);
	printf("center_matrix:");
	printf(">>>>>>>>>>>>>>>>>>center_in_view:%f,%f,%f\n",centerInView[0],centerInView[1],centerInView[2]);
	printMatrix(transMatrix);
	MultMatricesd(*transMatrix,getMatrixP(VIEW_MATRIX),*viewMatrix);
	printMatrix(viewMatrix);
	setViewMatrix(viewMatrix);
	RotationMatrix44d(*rotateMatrix, (x*1.0 - mouseX)/360*3.14, 0.0,1.0,0.0);
	MultMatricesd(*rotateMatrix,getMatrixP(VIEW_MATRIX),*viewMatrix);
	setViewMatrix(viewMatrix);
	RotationMatrix44d(*rotateMatrix,(y*1.0 - mouseY)/360*3.14, 1.0,0.0,0.0f);
	printMatrix(rotateMatrix);
	MultMatricesd(*rotateMatrix,getMatrixP(VIEW_MATRIX),*viewMatrix);
	setViewMatrix(viewMatrix);
	Translated(transMatrix,centerInView[0],centerInView[1],centerInView[2]);
	MultMatricesd(*transMatrix,getMatrixP(VIEW_MATRIX),*viewMatrix);
	setViewMatrix(viewMatrix);
	updateViewMatrix();
	glutPostRedisplay();
}

static GLdouble scaleFact = 1.0;

static void scale(int x, int y)
{
	GLdouble centerInModel[4],centerInView[4];
	GLdouble *matrixView,transMatrix[4][4],viewMatrix[4][4],scaleMatrix[4][4];
	GLfloat angleY,angleX;
	
	
	matrixView = getMatrixP(VIEW_MATRIX);
	centerInModel[0] = centerX,centerInModel[1] = centerY,centerInModel[2] =centerZ,centerInModel[3] = 1.0;
	//printf("<<<<<<<<<<<<<<<<<<@center_in_model>>>>>>>>>>>>>>>>>%f,%f,%f\n",centerInModel[0],centerInModel[1],centerInModel[2]);
	MultMatrixVecd(matrixView, centerInModel, centerInView);
	
	
	Translated(transMatrix,-centerInView[0],-centerInView[1],-centerInView[2]);
	//printf("center_matrix:");
	//printf(">>>>>>>>>>>>>>>>>>center_in_view:%f,%f,%f\n",centerInView[0],centerInView[1],centerInView[2]);
	printMatrix(transMatrix);
	MultMatricesd(*transMatrix,getMatrixP(VIEW_MATRIX),*viewMatrix);
	printMatrix(viewMatrix);
	setViewMatrix(viewMatrix);

	if( (y - mouseY) < 0)
		scaleFact = 1.05;
	else 
		if((y - mouseY ) > 0)
		scaleFact = 1.0/1.05;
	printf("SCALE_FACT:%d\n",y - mouseY);
	ScaleMatrix44d(*scaleMatrix, scaleFact,scaleFact,scaleFact);
	MultMatricesd(*scaleMatrix,getMatrixP(VIEW_MATRIX),*viewMatrix);
	setViewMatrix(viewMatrix);

	Translated(transMatrix,centerInView[0],centerInView[1],centerInView[2]);
	MultMatricesd(*transMatrix,getMatrixP(VIEW_MATRIX),*viewMatrix);
	setViewMatrix(viewMatrix);
	updateViewMatrix();
	glutPostRedisplay();
}

static void motionFunc(int x, int y)
{
	switch(mouse_state)
	{
	case MOUSE_B:
		//printf("move  X=%d,Y=%d\n", x, y);
		
		break;
	case MOUSE_C:
		rotate(x, y);
		//printf("rotate  X=%d,Y=%d\n", x, y);
		
		break;
	case MOUSE_E:
		printf("scale  X=%d,Y=%d\n", x, y);
		scale(x, y);
		break;
	case MOUSE_F:
		//printf("  X=%d,Y=%d\n", x, y);
		
		break;
	case MOUSE_A:
	case MOUSE_D:
	case MOUSE_NONE:
		//printf("nothing to do!!! X=%d,Y=%d\n", x, y);
		break;
	default:
		break;

	}
	mouseX = x,mouseY = y;
}






struct quads quads,quads1;
GLdouble vertex[] = {1,1,1,\
					-1,1,1,\
					-1,-1,1,\
					1,-1,1,
					1,1,-1,\
					-1,1,-1,\
					-1,-1,-1,\
					1,-1,-1};
GLdouble color[] = {0.2,0.6,0.8,0.5,\
					0.8,0.2,0.8,0.5,\
					0.8,0.5,0.2,0.5,\
					0.5,0.8,0.8,0.5,\
					0.8,0.6,0.2,0.2,\
					0.2,0.2,0.8,0.2,\
					0.8,0.5,0.2,0.2,\
					0.5,0.8,0.2,0.2\
					};
GLdouble normal[] = {1,1,1,\
					-1,1,1,\
					-1,-1,1,\
					1,-1,1,\
					1,1,-1,\
					-1,1,-1,\
					-1,-1,-1,\
					1,-1,-1};
GLdouble texture[] = { 0.0, 1, 1,1,1.0, 0.0, 0.0, 0.0, 1, 1, 0, 1, 0, 0, 1, 0};
GLuint index[] = {0,1,2,3,4,5,6,7
					};
GLuint index1[] = {1,5,6,2,4,0,3,7
					};

static void setup(void)
{
	
	init_pipe();
	
	pipeBindPositionLocation();
	PerspectiveM(ANGLE*1.0, 1.0, perspectiveNear, perspectiveFar, getMatrixP(PROJECT_MATRIX) );
	LookAtM(eyeX,eyeY,eyeZ,centerX, centerY, centerZ, upX, upY, upZ, getMatrixP(VIEW_MATRIX));
	pipeLinkProgram();
	quads_init(&quads, 2, vertex, 8, color, normal, texture, index);
	quads_init(&quads1, 2, vertex, 8, color, normal, texture, index1);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_LINE_SMOOTH);
	//glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);

}


static void display(void);

static void passiveMotionFunc(int x, int y)
{
	mouseX = x,mouseY = y;	
	if(x == mouseX && y == mouseY)
		return;
}





static void display(void)
{
	glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
	quads_draw(&quads);
	//glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);
	//quads_draw(&quads1);
	glutSwapBuffers();
}
int main(int argc, char *argv[])
{
	int i;
	glutInit(&argc,argv);
	glutInitDisplayMode(GLUT_DOUBLE|GLUT_RGB|GLUT_DEPTH);
	glutInitWindowSize(width,hight);//窗口大小
	//glutInitWindowSize(1300,600);
	//glutInitContextVersion(3,3);
	glutCreateWindow("hanjin_orbit");
	i = glewInit();// must after glutCreateWindow:::>>>>>>>>>>>
	glutDisplayFunc(display);
	glutReshapeFunc(changeSize);
	//glutKeyboardFunc(input);
	glutMouseFunc(mouseFunc);
	glutMotionFunc(motionFunc);
	glutPassiveMotionFunc(passiveMotionFunc);
	glClearColor(0.2f, 0.2f, 0.2f, 0.5);//设置背景颜色
	glViewport(0, 0, width,hight);
	setup();
	printf("VERSION:%s\n",glGetString(GL_VERSION));

	glutMainLoop();
	


	return 0;
}
