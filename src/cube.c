#include <GL/glew.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>
#include <math.h>

#include <stdlib.h>
#include <stdio.h>
#include "cube.h"

#define CONS 1
static void _createCubes(GLint countX, GLint countY, GLint countZ, void *data)
{
	GLfloat *_data =(GLfloat*)data;
	int i,j,k;
	for(i = 0; i < countX; i++)
	{
		for(j = 0; j < countY; j++)
		{
			for(k = 0; k < countZ; k++)
			{
				_data[k*countY*countX * 3 + j * countX *3 + i * 3 + 0] = i * CONS; 
				_data[k*countY*countX * 3 + j * countX *3 + i * 3 + 1] = j * CONS; 
				_data[k*countY*countX * 3 + j * countX *3 + i * 3 + 2] = k * -CONS; 
			}
		}
	}
	printf("data_init_OK\n");
}

GLfloat* createCubes(GLint countX, GLint countY, GLint countZ)
{
	void *data = malloc(countX * countY * countZ * 3 * sizeof(GLfloat));
	_createCubes(countX, countY, countZ, data);
	return (GLfloat*)data;
}

void deleteCubes(void *data)
{
	free(data);
}

