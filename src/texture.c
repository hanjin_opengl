#include <GL/glew.h>
#include <stdio.h>
#include <malloc.h>


#include "pipe_init.h"


#define TEXTURE_SIZE (TEXTURE_WIDTH * TEXTURE_HEIGHT * 3)
#define TEXTURE_WIDTH 206
#define TEXTURE_HEIGHT 206



GLuint textureName;
void texture_init(void)
{
	unsigned char *textures;
	size_t count = 0,read;
	FILE *fp = fopen("07.bmp","rb+");//06.mybmp�ļ���50*50����BGR24�洢
	textures = (unsigned char *)malloc(TEXTURE_SIZE+2);
	do
	{
		read = fread(textures+count,  1,TEXTURE_WIDTH*3, fp);
		count += read;
		fread(textures+count,1,2,fp);
	}while(read != 0);
	printf("count:%d",count);
	fclose(fp);
	glGenTextures(1, &textureName);
	glBindTexture(GL_TEXTURE_2D, textureName);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glPixelStorei(GL_UNPACK_ALIGNMENT,1);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, TEXTURE_WIDTH, TEXTURE_HEIGHT, 0, GL_BGR, GL_UNSIGNED_BYTE, textures);
	//glActiveTexture(GL_TEXTURE0);
	free(textures);

}