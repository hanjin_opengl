#include <GL/glew.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>
#include <math.h>
#include <stdio.h>

#include "pipe_init.h"

const char *vertexShaderSource[] ={
	"#version 330 \n",
	"in vec3 vertexCood; \n",
	"in vec2 textureCood; \n",
	"in vec3 normalCood; \n",
	"in vec4 vertexColor; \n",
	"out vec4 fragColor; \n",
	"smooth out vec2 varingTexCood; \n",	
	//"out vec4 position; \n",
	"out flat int ID; \n",
	"uniform vec4 cubeColor = vec4(0.5,0,0,0); \n",
	
	"uniform mat4 mMatrix = mat4(1.0f); \n",// ;
	"uniform mat4 vMatrix = mat4(1.0f); \n",// = mat4(1.0f);
	"uniform mat4 pMatrix = mat4(1.0f); \n",// = mat4(1.0f);
	"void main(void) \n",
	"{ \n",
	"vec4 vc; \n",
	"vc = vec4(vertexCood,1.0); \n",
	"gl_Position = pMatrix * vMatrix * mMatrix * vc; \n",
	"varingTexCood = textureCood; \n",
	"fragColor = vertexColor; \n",
	"ID = gl_VertexID; \n",
	"}"
};

const char *fragShaderSource[] ={
	"#version 330 \n",
	"uniform sampler2D colorMap ; \n"
	"uniform int useTexture = int(0); \n",
	"in vec4 fragColor; \n",
	"in vec2 varingTexCood; \n",
	"in flat int ID; \n",
	"out vec4 id; \n",
	"out vec4 color; \n",
	"void main(void) \n",
	"{ \n",
	"//vec3 tcolor;",
	"id = vec4(gl_PrimitiveID,-100,10,0); \n",
	"if(useTexture == 1) \n"
	"{ color = texture(colorMap, varingTexCood.st); \n",
	"color.a = 0.5;\n",
	"}\n",
	"else \n"
	"color = fragColor; \n",
	"}"
};

GLuint vertexShader;
GLint mMatrixLocation;
GLint vMatrixLocation;
GLint pMatrixLocation;
GLuint fragShader;
GLint cubeColorLocation;
GLuint program;
GLint useTextureLocation;
GLint colorMapLocation;

GLdouble md[4][4]={
	{1.0, 0.0, 0.0, 0.0},
	{0.0, 1.0, 0.0, 0.0},
	{0.0, 0.0, 1.0, 0.0},
	{0.0, 0.0, 0.0, 1.0}};
GLdouble vd[4][4]={
	{1.0, 0.0, 0.00, 0.0},
	{0.00, 1.0, 0.0, 0.0},
	{0.00, 0.0, 1.0, 0.0},
	{0.00, 0.0, 0.0, 1.0}};
GLdouble pd[4][4]={          //-2,2,-2,2,2,6
	{1.0, 0.00, 0.0, 0.00},
	{0.00, 1.0, 0.0, 0.00},
	{0.00, 0.00, 1.0, 0.0},
	{0.00, 0.00, 0.0, 1.00}
};

GLdouble *p[3]= {&md[0][0],&vd[0][0],&pd[0][0]};

GLdouble* getMatrixP(enum matrixType type)
{
	return p[type];
}

void init_pipe(void)
{
	GLint test;

	vertexShader = glCreateShader(GL_VERTEX_SHADER);
	fragShader = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(vertexShader, sizeof(vertexShaderSource)/sizeof(char *), vertexShaderSource, NULL);
	glShaderSource(fragShader, sizeof(fragShaderSource)/sizeof(char *), fragShaderSource, NULL);
	glCompileShader(vertexShader);
	glGetShaderiv(vertexShader, GL_COMPILE_STATUS, &test);
	if(test == GL_FALSE)
	{
		char log[1024];
		glGetShaderInfoLog(vertexShader, 1024, NULL, log);
		printf("vertexShader_log:%s\n", log);
	}
	glCompileShader(fragShader);
	glGetShaderiv(fragShader, GL_COMPILE_STATUS, &test);
	if(test == GL_FALSE)
	{
		char log[1024];
		glGetShaderInfoLog(fragShader, 1024, NULL, log);
		printf("fragShader_log:%s\n", log);
	}
	program = glCreateProgram();
	if(program < 0)
	{
		exit(1);
	}
	glAttachShader(program, vertexShader);
	glAttachShader(program, fragShader);
	
}




void pipeBindPositionLocation()
{
	glBindAttribLocation(program, 0, "vertexCood");
	glBindAttribLocation(program, 1, "vertexColor");
	glBindAttribLocation(program, 2, "normalCood");
	glBindAttribLocation(program, 3, "textureCood");
	
	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);
	glEnableVertexAttribArray(2);
	glEnableVertexAttribArray(3);
}
#ifdef DEBUG
void printMatrix(GLdouble m[4][4])
{
	int i = 0,j=0;
	for(;i<4; i++)
	{
		for(j=0;j<4; j++)
			printf("%f ",m[i][j]);
		printf("\n");
	}
}
#endif

void pipeLinkProgram(void)
{
	GLint test;
	GLenum error;
	char mlog[1024];
	glBindFragDataLocation(program, 0,"color");
	error = glGetError();
	if(error == GL_NO_ERROR){printf("PIPELINLPROGRAM_GOOD\n");}
	glBindFragDataLocation(program, 1,"id");
	glLinkProgram(program);//must after data bound 
	glGetProgramiv(program, GL_LINK_STATUS, &test);
	if(test == GL_FALSE)
	{
		
		glGetProgramInfoLog(program, 1024, NULL, mlog);
		printf("linkProgram_log:%s\n", mlog);
		glDeleteShader(vertexShader);
		glDeleteShader(fragShader);
		glDeleteProgram(program);
		//exit(12);
	}
	
	test = glGetFragDataLocation(program,"id");
	printf("ID_LOCATION:%d\n",test);
	glUseProgram(program);
	glGetProgramiv(program, GL_LINK_STATUS, &test);
	if(test == GL_FALSE)
	{
		//char mlog[1024];
		glGetProgramInfoLog(program, 1024, NULL, mlog);
		printf("linkProgram_log:%s\n", mlog);
		glDeleteShader(vertexShader);
		glDeleteShader(fragShader);
		glDeleteProgram(program);
		//exit(13);
	}

	mMatrixLocation = glGetUniformLocation(program, "mMatrix");
	if(mMatrixLocation == -1)
		printf("glGetUniformLocation(program, \"mMatrix\")\n", mlog);
	else
		glUniformMatrix4dv(mMatrixLocation, 1, GL_FALSE, &md[0][0]);
	vMatrixLocation = glGetUniformLocation(program, "vMatrix");
	if(vMatrixLocation == -1)
		printf("glGetUniformLocation(program, \"vMatrix\")\n", mlog);
	else
		glUniformMatrix4dv(vMatrixLocation, 1, GL_FALSE, &vd[0][0]);
	pMatrixLocation = glGetUniformLocation(program, "pMatrix");
	if(pMatrixLocation == -1)
		printf("glGetUniformLocation(program, \"pMatrix\")\n", mlog);
	else		
		glUniformMatrix4dv(pMatrixLocation, 1, GL_FALSE, &pd[0][0]);
	cubeColorLocation = glGetUniformLocation(program, "cubeColor");
	if(cubeColorLocation == -1)
		printf("glGetUniformLocation(program, \"cubeColor\")\n", mlog);
	else	
		glUniform4f( cubeColorLocation, 1.0, 0.0, 0.0, 1.0);

	useTextureLocation = glGetUniformLocation(program, "useTexture");
	if(useTextureLocation == -1)
		printf("glGetUniformLocation(program, \"useTextureLocation\")\n", mlog);
	else	
		glUniform1i( useTextureLocation, 1);

	colorMapLocation = glGetUniformLocation(program, "colorMap");
	if(colorMapLocation <= 0)
		printf("glGetUniformLocation(program, \"colorMapLocation\")\n", mlog);
	else	
		glUniform1i( colorMapLocation, 0);
	texture_init();
	//printMatrix(vd);
}

void updateModelMatrix()
{
	glUniformMatrix4dv(mMatrixLocation, 1, GL_FALSE, &md[0][0]);
}
void updateViewMatrix()
{
	glUniformMatrix4dv(vMatrixLocation, 1, GL_FALSE, &vd[0][0]);
}
void updateProjectMatrix()
{
	glUniformMatrix4dv(pMatrixLocation, 1, GL_FALSE, &pd[0][0]);
}

struct M{
	GLdouble m[16];
}; 

void setViewMatrix(GLdouble m[4][4])
{
	struct M *mp = (struct M*)vd;
	*mp = *(struct M*)m;
}

void fboBind(void)
{
	GLenum status;
	pipeLinkProgram();
	status = glCheckFramebufferStatus(GL_DRAW_FRAMEBUFFER);
	if(status !=  GL_FRAMEBUFFER_COMPLETE)
		printf("FBO_ERROR\n");
	//GLenum fbobuffers = {};


}
	
