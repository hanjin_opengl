#include <GL/glew.h>
#include "quads.h"

void quads_init(struct quads *object, GLuint primitiveCount,\
				 const GLdouble *vertex, GLuint vertexCount,\
				 const GLdouble *color, const GLdouble *normal,\
				 const GLdouble *texture, const GLuint *index)

{
	glGenVertexArrays(1,&object->vao);
	object->inited = 1;
	glBindVertexArray(object->vao);
	if(vertex)
	{
		glGenBuffers(1,&object->vertex);
		object->vertex_inited =1;
		glBindBuffer(GL_ARRAY_BUFFER,object->vertex);
		glBufferData(GL_ARRAY_BUFFER, vertexCount * sizeof(GLdouble) * 3, vertex, GL_STATIC_DRAW);
		
		glEnableVertexAttribArray(0);glVertexAttribPointer(0, 3, GL_DOUBLE, GL_FALSE, 0, 0);
	}
	if(color)
	{
		glGenBuffers(1,&object->color);
		object->color_inited =1;
		glBindBuffer(GL_ARRAY_BUFFER,object->color);
		glBufferData(GL_ARRAY_BUFFER, vertexCount * sizeof(GLdouble) * 4, color, GL_STATIC_DRAW);
		
		glEnableVertexAttribArray(1);glVertexAttribPointer(1, 4, GL_DOUBLE, GL_FALSE, 0, 0);
	}
	if(normal)
	{
		glGenBuffers(1,&object->normal);
		object->normal_inited =1;
		glBindBuffer(GL_ARRAY_BUFFER,object->normal);
		glBufferData(GL_ARRAY_BUFFER, vertexCount * sizeof(GLdouble) * 3, normal, GL_STATIC_DRAW);
		
		glEnableVertexAttribArray(2);glVertexAttribPointer(2, 3, GL_DOUBLE, GL_TRUE, 0, 0);
	}
	if(texture)
	{
		glGenBuffers(1,&object->texture);
		object->texture_inited =1;
		glBindBuffer(GL_ARRAY_BUFFER,object->texture);
		glBufferData(GL_ARRAY_BUFFER, vertexCount * sizeof(GLdouble) * 3, texture, GL_STATIC_DRAW);
		
		glEnableVertexAttribArray(3);glVertexAttribPointer(3, 2, GL_DOUBLE, GL_FALSE, 0, 0);
	}
	if(index)
	{
		glGenBuffers(1,&object->index);
		object->index_inited =1;
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,object->index);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, primitiveCount * sizeof(GLuint) * 4, index, GL_STATIC_DRAW);
	}
	object->primitive_count = primitiveCount;

	glBindVertexArray(0);
}

void quads_draw(struct quads* object)
{
	glBindVertexArray(object->vao);
	glDrawElements(GL_QUADS,object->primitive_count * 4,GL_UNSIGNED_INT, 0);
	
	glPointSize(3);
	glDrawArrays(GL_POINTS, 0, 8);
	glBindVertexArray(0);
}