#ifndef __DATA_INIT_H
#define __DATA_INIT_H

#ifdef __cplusplus
extern "C"{
#endif


#include <GL/gl.h>
GLfloat* createCubes(GLint countX, GLint countY, GLint countZ);

void deleteCubes(void *data);



#ifdef __cplusplus
}
#endif

#endif