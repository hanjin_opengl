#include <GL/glew.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>

#include "VAO_init.h"

static GLuint vao;
GLuint createVAO(void)
{
	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);
	return vao;
}
